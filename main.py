######### IMPORTANT
# logged file format
# timestamp, side, size, price, grossValue, homeNotional, foreignNational


from bitmex_websocket import BitMEXWebsocket
import logging
from time import sleep
import urllib.parse
import logging.handlers
# Basic use of websocket.
def run():
    logger = setup_logger()

    # Instantiating the WS will make it connect. Be sure to add your api_key/api_secret.
    ws = BitMEXWebsocket(endpoint="wss://www.bitmex.com/realtime", symbol="XBTUSD",
                         api_key=None, api_secret=None)

    #logger.info("Instrument data: %s" % ws.get_instrument())

    # Run forever
    while(ws.ws.sock.connected):
        #logger.info("Ticker: %s" % ws.get_ticker())
        #if ws.api_key:
         #   logger.info("Funds: %s" % ws.funds())
        # logger.info("Market Depth: %s" % ws.market_depth())
        my_list = ws.recent_trades()
        for i in range(len(my_list)):
            logger.info("{}, {}, {}, {}, {}, {}, {}".format(my_list[i]['timestamp'], my_list[i]['side'], my_list[i]['size'], my_list[i]['price'], my_list[i]['grossValue'], my_list[i]['homeNotional'], my_list[i]['foreignNotional']))
        sleep(10)


def setup_logger():
    # Prints logger info to terminal
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)  # Change this to DEBUG if you want a lot more info
    #ch = logging.handlers.RotatingFileHandler(savefilename, maxBytes=5*1024, backupCount=100)
    ch = logging.handlers.TimedRotatingFileHandler('BitMEX.dump', when='h', backupCount=100)
    # create formatter
    formatter = logging.Formatter("%(message)s")
    # add formatter to ch
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


if __name__ == "__main__":
    run()
